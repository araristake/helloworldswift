//
//  ViewController.swift
//  HelloWorld
//
//  Created by Armen Aristakesyan on 14/12/2020.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tirageLabel: UILabel!
    let tirage: Tirage = Tirage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func tirageAlea(_ sender: UIButton) {
        tirageLabel.text = tirage.description
    }
}

