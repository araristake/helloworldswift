//
//  Tirage.swift
//  HelloWorld
//
//  Created by Armen Aristakesyan on 14/12/2020.
//

import Foundation

class Tirage : CustomStringConvertible {
    
    /**
     Array of numbers.
     */
    var numeros: [Int] = []
    
    /**
     Construction of Tirage object.
     */
    init() {
        self.populate()
    }
    
    /**
     Overriding description field.
     */
    var description: String {
        var res: String = "Numéros : "
        let(nums, chance) = self.tirage()
        for num in nums {
            res += String(num) + " "
        }
        res += "\n\nChance : " + String(chance)
        return res
    }
    
    /**
     Pulling 5 numbers from number array and chance number.
     */
    func tirage() -> (nums : [Int], chance :Int) {
        var nums: [Int] = []
        let chance: Int = Int.random(in: 1..<11)
        for _ in 0...5 {
            let rand: Int = Int.random(in: 0..<self.numeros.count)
            nums.append(self.numeros[rand]);
            self.numeros.remove(at: rand)
        }
        nums.sort()
        self.reset()
        return (nums, chance)
    }
    
    /**
     Reset the number array for the next pull.
     */
    func reset() -> Void {
        self.numeros.removeAll()
        self.populate()
    }
    
    /**
     Populate thenumber array.
     */
    func populate() -> Void {
        for i in 1...49 {
            self.numeros.append(i);
        }
    }
}
